<?php
/*CommandService.php
Universal command methods for the Symfony template
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace CYINT\ComponentsPHP\Services;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class CommandService
{
    protected $app_directory;

    public function __construct($app_directory)
    {
        $this->app_directory = $app_directory;
    }

    public function executeSynchronousCommand($command, $timeout = 3600, $idle_timeout = 3600)
    {
        $Process = $this->createProcess($command);
        $Process->setTimeout($timeout);
        $Process->setIdleTimeout($idle_timeout);
        $Process->mustRun();
    }

    public function executeAsynchronousCommand($command)
    {
        $Process = $this->createProcess($command);
        $Process->start();
    }

    public function createProcess($command)
    {
        $Process = new Process($command);
        $Process->setWorkingDirectory($this->app_directory);
        return $Process;       
    }
}
